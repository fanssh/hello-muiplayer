module.exports = [
    {
        text: 'Guide',
        link: '/guide/'
    },
    {
        text:'Demo',
        link: '/demo/'
    },
    {
        text:'About',
        ariaLabel: 'About',
        items:[
            {
                text:'Join us',
                link: '/joinUs/'
            },
            {
                text: 'Submit issues',
                link: 'https://github.com/muiplayer/hello-muiplayer/issues'
            },
        ]
    }
]
