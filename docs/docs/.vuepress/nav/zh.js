module.exports = [
    {
        text: '指南',
        link: '/zh/guide/'
    },
    {
        text:'演示',
        link: '/zh/demo/'
    },
    {
        text:'关于',
        ariaLabel: '关于',
        items:[
            {
                text:'加入我们',
                link: '/zh/joinUs/'
            },
            {
                text: '提交 Issues',
                link: 'https://github.com/muiplayer/hello-muiplayer/issues'
            },
        ]
    }
]
