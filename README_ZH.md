<div align="center"><img src="https://muiplayer.oss-cn-shanghai.aliyuncs.com/static/image/logo.png" width="150px"></div>
<div align="center">
    <a href="https://www.npmjs.com/package/mui-player" target="_blank"><img src="https://img.shields.io/badge/npm-v1.4.0-blue" /></a>
    <a href="https://www.npmjs.com/package/mui-player-desktop-plugin" target="_blank"><img src="https://img.shields.io/badge/npm%20player%20desktop-v1.3.0-blue" /></a>
	<a href="https://www.npmjs.com/package/mui-player-mobile-plugin" target="_blank"><img src="https://img.shields.io/badge/npm%20player%20mobile-v1.3.0-blue" /></a>
	<a href="https://github.com/muiplayer/hello-muiplayer/tree/master/dist/js" target="_blank"><img src="https://img.shields.io/badge/gzip%20size-15kb-brightgreen" /></a>
    <a href="https://github.com/muiplayer/hello-muiplayer/blob/master/LICENSE" target="_blank"><img src="https://img.shields.io/badge/license-MIT-brightgreen" /></a>
</div>

<h1 align="center">Mui Player</h1>

![Desktop](https://muiplayer.oss-cn-shanghai.aliyuncs.com/static/image/desktopPreview.png)

![Mobile](https://muiplayer.oss-cn-shanghai.aliyuncs.com/static/image/mobile_preview.png)

<a href="https://muiplayer.js.org/" target="_blank">Docs</a> | <a href="https://muiplayer.js.org/zh/" target="_blank">中文文档</a>

## 介绍

MuiPlayer 是一款 HTML5 视频播放插件，其默认配置了精美可操作的的播放控件，涉及了常用的播放场景，例如全屏播放、播放快进、循环播放、音量调节、视频解码等功能。

支持 mp4、m3u8、flv 等多种媒体格式播放，解决大部分兼容问题，同时适应在PC、手机端播放。

MuiPlayer 具有丰富的参数可以自定义播放器实例，通过轻松的配置即可完成自定义场景的视频播放。

## 特点

MuiPlayer 帮助我们解决了日常 H5 Video 应用开发中的常见的一些大量问题：

1. 各浏览器平台播放 ui 不能统一
2. ui 扩展之间以及状态处理容易产生冲突
3. 在不同环境下（android、ios、pc）针对 h5 video api 可能触发事件的时机尽不相同
4. 媒体格式存在各种兼容问题，muiplayer 处理了大多数在不同环境下播放的兼容问题
5. 重复踩踏在开发 h5 video 过程中的一些坑，我们提供了一套完好的解决方案，让开发者少走一些弯路

## 安装

使用 npm 安装：

```
npm i mui-player --save
```

使用 yarn 安装：

```
yarn add mui-player
```

## 使用

1、使用 script 标签引入：

```html
<!-- import basic style files mui-player.min.css -->
<link rel="stylesheet" type="text/css" href="css/mui-player.min.css"/>

<!-- import basic script mui-player.min.js -->
<script type="text/javascript" src="js/mui-player.min.js"></script>

<!-- Specify the player container -->
<div id="mui-player"></div>
```

或者使用模块管理器引入：

```js
import 'mui-player/dist/mui-player.min.css'
import MuiPlayer from 'mui-player'
```

2、定义播放器容器：

```html
<div id="mui-player"></div>
```

3、初始化构建播放器：

```js
// 初始化 MuiPlayer 插件，MuiPlayer 方法传递一个对象，该对象包括所有插件的配置
var mp = new MuiPlayer({
    container:'#mui-player',
    title:'Title',
    src:'./static/media/media.mp4',
})
```

以上就能为初始化构建一个具有默认配置控件的视频播放器，下面你可以阅读关于 MuiPlayer 的一些 API 基础配置选项。前往 [参数API](https://muiplayer.js.org/zh/api/)

## 开始

启动此工程

```
npm install
npm start
```

## Plugins

- [mui-player-mobile-plugin.js](https://www.npmjs.com/package/mui-player-mobile-plugin)
- [mui-player-desktop-plugin.js](https://www.npmjs.com/package/mui-player-desktop-plugin)

## Stargazers

[![Stargazers repo roster for @muiplayer/hello-muiplayer](https://reporoster.com/stars/muiplayer/hello-muiplayer)](https://github.com/muiplayer/hello-muiplayer/stargazers)

## Forkers

[![Forkers repo roster for @muiplayer/hello-muiplayer](https://reporoster.com/forks/muiplayer/hello-muiplayer)](https://github.com/muiplayer/hello-muiplayer/network/members)